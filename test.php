<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attendance;
use App\Models\Employee;
use App\Models\Hours;
use App\Models\Deduction;
use Illuminate\Support\Facades\DB;

class AttendanceCtrl extends Controller
{
    public $pageTitle = 'Monitoring Kehadiran';
    public $pageSubtitle = 'Monitoring Kehadiran';
    public $leftMenuActive = 'attendance/index';
    public function index(Request $request){
        // dd('asd');
        $data['pageTitle'] = '';
        return view('Admin.attendance', compact('data'));
    }
    public function absen(Request $request){
        $responseCode = 'ok';
        $message = '';
        DB::beginTransaction();
        try{
            $employee = Employee::where('nip',$request->nip)->first();
            $hours = Hours::where('days',date('w'))->where('shift', $request->shift)->first();
            $ddc = Deduction::get();
            // dd($hours);
            if($hours == null){
                return response()->json([
                    'responseCode'  => 'error',
                    'message'       => 'Belum Setting Jam Absen' 
                ], 200);
            }
            $deduction = 0;
            $model  = Attendance::where('employee_nip',$request->nip)->whereNotNull('hours_in')
            ->where('dates', date('Y-m-d'))->first();
            if($request->type == 'masuk'){
                if($model == null){
                    if(strtotime($hours->hours_in) < strtotime($request->jam)){
                        // $deduction = $employee->m_allowance + $employee->t_allowance;
                        foreach($ddc as $a){
                            if($a->type == "x_other"){
                                $deduction +=  $a->amount;
                            }else{
                                $deduction += $employee->{$a->type};
                            }
                            
                        }
                    }
                    $model                  = new Attendance();
                    $model->employee_nip    = $request->nip;
                    $model->dates           = date('Y-m-d');
                    $model->shift           = $request->shift;
                    $model->hours_in        = $request->jam;
                    $model->deduction       = $deduction;
                    $model->save();

                    $message = 'Sukses Absen Masuk';
                    // dd($deduction);
                }else{
                    $responseCode = 'error';
                    $message = 'Sudah Melakukan Absen Masuk';
                }
            }else{
                if($model == null){
                    $model                  = new Attendance();
                    $deduction = $employee->m_allowance + $employee->t_allowance;
                    $model->hours_in        = "09:00:00";
                }else{
                    if(strtotime($hours->hours_out) > strtotime($request->jam)){
                        // $deduction = $employee->m_allowance + $employee->t_allowance;
                    }
                }
                $model->employee_nip    = $request->nip;
                $model->dates           = date('Y-m-d');
                $model->shift           = $request->shift;
                $model->hours_out       = $request->jam;
                $model->deduction       = $deduction;
                $model->save();
                $message = 'Sukses Absen Keluar';
            }
            DB::commit();
            return response()->json([
                'responseCode'  => $responseCode,
                'message'       => $message 
            ], 200);
        }
        catch(\Exception $e){
            return response()->json([
                'responseCode'  => 'error',
                'message'       =>$e->getMessage()
            ], 401);
        }
    }
    public function search(Request $request){
        $model  = Attendance::where('employee_nip', $request->employee)
        ->whereRaw('MONTH(dates) = '.$request->months.' and Year(dates) = '.$request->years)
        ->orderBy('dates')
        ->get();
        $table_all = '';
        foreach($model as $i => $x){
            // var_dump($x);die;
            $shift1 = $x->shift == 1 ? "selected" : "";
            $shift2 = $x->shift == 2 ? "selected" : "";
            $table_all .= "<tr data-index = '".$i."'>";
            $table_all .= "<td>".($i+1)."</td>";
            $table_all .= "<td><input type='hidden' id='id' value='".$x->id."'><input type='text' class='form-control datepickers editing' id='dates' value='".$this->changedates2($x->dates)."' disabled></td>";
            $table_all .= "<td><select class='form-control editing' id='shift' disabled><option value='1' ".$shift1.">Shift 1</option><option value='2' ".$shift2.">Shift 2</option></select></td>";
            $table_all .= "<td><input type='text' class='form-control timepicker editing' id='hours_in' value='".$x->hours_in."' disabled></td>";
            $table_all .= "<td><input type='text' class='form-control timepicker editing' id='hours_out' value='".$x->hours_out."' disabled></td>";
            $table_all .= "<td><input type='text' class='form-control numbers' id='deduction' value='".$x->deduction."' disabled></td>";
            $table_all .= "<td><button id='edit' class='btn btn-primary'><i class='fa fa-edit'></i></button><button id='save' class='btn btn-success save'><i class='fa fa-check'></i></button></td>";
            $table_all .= "<tr>";
        }
        $datas['data'] = $table_all;
        return $datas;
    }
    public function changedates($dates){
        // dd($dates);
        $x = explode("/",$dates);
        return $x[2]."-".$x[1]."-".$x[0];
    }
    public function changedates2($dates){
        // dd($dates);
        $x = explode("-",$dates);
        return $x[2]."/".$x[1]."/".$x[0];
    }
    public function action(Request $request){
        // dd($request);
        $days = date('w', strtotime($this->changedates($request->dates)));
        $hours = Hours::where('days',$days)->where('shift', $request->shift)->first();
        $employee = Employee::where('nip',$request->employee)->first();
        $ddc = Deduction::get();
        $deduction = 0;
        try{
            if(strtotime($hours->hours_in) < strtotime($request->hours_in)){
                foreach($ddc as $a){
                    if($a->type == "x_other"){
                        $deduction +=  $a->amount;
                    }else{
                        $deduction += $employee->{$a->type};
                    }
                    
                }
            }
            if($request->id == null ){
                $exist      = Attendance::where('employee_nip', $request->employee)
                            ->where('dates', $this->changedates($request->dates))
                            ->where('shift', $request->shift)
                            ->first();
                // dd($exist);
                if($exist){
                    return response()->json([
                        'responseCode'  => 'error',
                        'message'       => 'Data Sudah Pernah di Inputkan',
                        'deduction'     => $deduction,
                    ], 200);
                }
                $model              = new Attendance();
                $msg                = 'Tambah';
            }else{
                $model              = Attendance::where('id', $request->id)->first();
                $msg                = 'Update';
            }
            
            $model->hours_in        = $request->hours_in;
            $model->hours_out       = $request->hours_out;
            $model->shift           = $request->shift;
            $model->employee_nip    = $request->employee;
            $model->dates           = $this->changedates($request->dates);
            $model->deduction       = $deduction;
            $model->save();
            DB::commit();
            
            return response()->json([
                'responseCode'  => 'ok',
                'message'       => 'sukses '.$msg.' kehadiran',
                'deduction'     => $deduction,
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'responseCode'  => 'error',
                'message'       =>$e->getMessage()
            ], 401);
        }
        dd($hours);
    }
}
